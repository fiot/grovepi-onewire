# grovepi-onewire

## Installing the firmware

### Via pre-compiled firmware

- copy 'firmware/firmware.hex' from this repo onto your Raspbian for Robots distro as '~/Desktop/GrovePi/Firmware/grove_pi_firmware.hex'
- on Raspbian for Robots RPi, run '~/Desktop/GrovePi/Firmware/firmware_update.sh'

### Compiling the firmware yourself 

- one can first use 'ino' to build the source code from the 'src' directory -- which will generate a firmware.hex file, which one can load onto the GrovePi shield as above.

## Using Python

- 'python/grovepi.py' should replace '~/Desktop/GrovePi/Software/Python/grovepi.py' on Raspbian for Robots
- 'python/grove_onewire_read.py' should be added to the '~/Desktop/GrovePi/Software/Python/' directory in Raspbian for Robots

## Current status 
- [Video](https://www.youtube.com/watch?v=uOFkEdpRhWM) of first demo
- Occasionally seems to produce junk values. Likely something to do with the time taken to convert the 1wire value.  Might require storing value in firmware and adding another loop.
